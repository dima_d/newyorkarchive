package com.kris.newyorkarchive

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    companion object {
        const val ATTR_FILENAME = "FILENAME"
    }

    lateinit var preferences: SharedPreferences
    private var counter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main)
        preferences = getPreferences(Context.MODE_PRIVATE)
        loadFile(preferences.getString(ATTR_FILENAME, ""))
        checkWarning()

        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)

        nyBigLogo.setOnClickListener(this::setupClicker)
        qrButton.setOnClickListener(this::initiateScan)
        textToFind.setOnFocusChangeListener { _, focus -> if (focus) startActivity(Intent(this, FindActivity::class.java)) }
    }

    private fun initiateScan(view: View) {
        IntentIntegrator(this).initiateScan()
    }

    private fun setupClicker(view: View) {
        counter++
        if (counter > 10) {
            counter = 0
            val fileDialog = OpenFileDialog(this)
            fileDialog.show()
            fileDialog.setOpenDialogListener {
                preferences.edit().putString(ATTR_FILENAME, it).apply()
                loadFile(it)
                val count = DataContainer.data?.size ?: 0
                Toast.makeText(this, "Добавлено $count записей", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (scanResult != null) {
            startActivity(Intent(this, FindActivity::class.java).putExtra(FindActivity.EXTRA_CONTENT, scanResult.contents))

        }

    }

    override fun onResume() {
        super.onResume()
        nyBigLogo.requestFocus()
        counter = 0
    }

    override fun onBackPressed() {}

    fun checkWarning() {
        if (DataContainer.data?.size ?: 0 == 0) {
            warning.visibility = View.VISIBLE
        } else {
            warning.visibility = View.GONE
        }
    }

    fun loadFile(filename: String) {
        if (filename.isEmpty()) {
            return
        }

        try {
            DataContainer.data = unpackZip(filename)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        checkWarning()
    }

}
