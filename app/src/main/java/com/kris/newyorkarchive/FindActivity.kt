package com.kris.newyorkarchive

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import kotlinx.android.synthetic.main.find_fragment.*
import kotlinx.android.synthetic.main.find_results_activity.*
import java.util.*

/**
 * TODO:Add javadoc
 */
class FindActivity : AppCompatActivity() {
    private val adapter = Adapter()
    companion object {
        const val EXTRA_CONTENT = "EXTRA_CONTENT"
    }

    val keywords = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.find_results_activity)

        textToFind.requestFocus()
        textToFind.addTextChangedListener(Watcher())

        recyclerView.layoutManager = LinearLayoutManager(this)

        recyclerView.adapter = adapter

        intent?.extras?.let {
            val string = it.getString(EXTRA_CONTENT, "")
            textToFind.setText(string)
        }


    }


    private inner class Adapter : RecyclerView.Adapter<ViewHolder>() {
        var data: ArrayList<ArchiveRow>? = null
            set(value) {
                field = value
                notifyDataSetChanged()
            }

        override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
            try {
                holder?.bindView(data?.get(position))
            } catch (e: IndexOutOfBoundsException) {
            }
        }

        override fun getItemCount(): Int = data?.size ?: 0

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
                = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.result_card, parent, false))

    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(record: ArchiveRow?) {
            itemView.findViewById<TextView>(R.id.cardText).text = record?.text
            itemView.findViewById<TextView>(R.id.cardTitle).text = record?.getKeyString(keywords)
        }
    }


    inner class Watcher : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(textToFind: CharSequence?, start: Int, before: Int, count: Int) {
            val stringTokenizer = StringTokenizer(textToFind.toString())
            keywords.clear()
            while (stringTokenizer.hasMoreTokens()) {
                val nextToken = stringTokenizer.nextToken()
                if (!nextToken.trim().isEmpty())
                    keywords.add(nextToken.trim().toLowerCase())
            }

            var data = ArrayList<ArchiveRow>()
            DataContainer.data?.forEach {
                if (it.checkKeywords(keywords)) {
                    data.add(it)
                }
            }

            adapter.data = data

        }
    }

}