package com.kris.newyorkarchive

import java.io.*
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParser
import android.util.Log
import org.xmlpull.v1.XmlPullParserFactory
import java.lang.StringBuilder
import java.util.*
import kotlin.collections.ArrayList


const val TAG = "XMLParcer"

fun unpackZip(file: String): ArrayList<ArchiveRow>? {
    val zis: ZipInputStream
    try {
        zis = ZipInputStream(BufferedInputStream(FileInputStream(file)))
        var ze: ZipEntry?

        do {
            ze = zis.nextEntry
        } while (ze != null && ze.name != "content.xml")

        val data = parceXmlData(zis)

        zis.closeEntry()
        zis.close()
        zis.close()
        return data
    } catch (e: IOException) {
        e.printStackTrace()
        return null
    }
}


private fun parceXmlData(inputStream: InputStream): ArrayList<ArchiveRow> {

    val archive = arrayListOf<ArchiveRow>()

    var row: ArrayList<String>? = null
    var cell = StringBuilder()


    try {
        val factory = XmlPullParserFactory.newInstance()
        factory.isNamespaceAware = true
        val xpp = factory.newPullParser()
        xpp.setInput(inputStream, "UTF8")

        while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
            when (xpp.getEventType()) {
                XmlPullParser.START_DOCUMENT -> Log.d(TAG, "START_DOCUMENT")
                XmlPullParser.START_TAG -> {
                    when (xpp.name) {
                        "table-row" -> {
                            row = ArrayList()
                        }
                        "table-cell" -> {
                            cell.setLength(0)

                        }

                    }
                }
                XmlPullParser.END_TAG -> {
                    when (xpp.name) {
                        "table-cell" -> {
                            val element = cell.toString()
                            if (!element.trim().isEmpty())
                                row?.add(element.trim().toLowerCase())

                        }
                        "table-row" -> {
                            if (row != null && (row.size >= 3)) {

                                try {
                                    val i = Integer.parseInt(row.get(0))
                                    val stringTokenizer = StringTokenizer(row[1])
                                    val keywords = ArrayList<String>()
                                    while (stringTokenizer.hasMoreTokens()) {
                                        val nextToken = stringTokenizer.nextToken()
                                        if (!nextToken.trim().isEmpty())
                                            keywords.add(nextToken.trim().toLowerCase())
                                    }
                                    archive.add(ArchiveRow(i, row[2], keywords))
                                } catch (e: Exception) {
                                }
                            }
                        }
                    }
                }
                XmlPullParser.TEXT -> cell.append(xpp.text).append(" ")
                else -> {
                }
            }
        
            xpp.next()
        }
        Log.d(TAG, "END_DOCUMENT")
    } catch (e: XmlPullParserException) {
        e.printStackTrace()
    } catch (e: IOException) {
        e.printStackTrace()
    }

    archive.forEach { Log.d(TAG, "row: : $it"); }

    return archive

}