package com.kris.newyorkarchive

import java.util.ArrayList

/**
* TODO: Add javadoc
*/


data class ArchiveRow(val coutn:Int, val text:String, val keyWords:List<String>){
    fun checkKeywords(keys:List<String>):Boolean{
        var count = 0
        keys.forEach {
            if(keyWords.contains(it)){
                count++
            }
        }

        return count>=coutn
    }

    override fun toString(): String {
        return "ArchiveRow(coutn=$coutn, keyWords=$keyWords, text='$text')"
    }

    fun getKeyString(keywords: ArrayList<String>) = keywords.filter { keyWords.contains(it) }.joinToString { it }


}